package aprobadosuspendido;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author LuisL
 */
public class AprobadoSuspendido {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Teclea tu calificacion: ");
        int nota = Integer.parseInt(br.readLine());

        if (nota >= 0 && nota <= 10) 
        {

            switch (nota) {
                case 5:
                    System.out.println("Aprobado");
                    break;
                case 6:
                    System.out.println("Bien");
                    break;
                case 7:
                    System.out.println("Mejor");
                    break;
                case 8:
                    System.out.println("Notable");
                    break;
                case 9:
                    System.out.println("Sobresaliente");
                    break;
                case 10:
                    System.out.println("Perfecto");
                    break;
                default:
                    System.out.println("Suspendido");
                    break;
           }
        }
        else
            System.out.println("ERROR: El valor es incorrecto.");

    }
    
}




